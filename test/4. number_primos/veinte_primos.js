function veinte_primos(number) {
    let acum = 0;
    
    if (number > 9 & number < 20){
        for (let i = 1; i <= number; i++){
            if (number % i == 0){
                acum = acum + 1;

            }
        } 

        if (acum == 2 || number == 2){
            return 'es primo';
        } else {
            return 'no es primo';
        }

    } else {
        return 'fuera de rango';
    }

}
module.exports = veinte_primos;