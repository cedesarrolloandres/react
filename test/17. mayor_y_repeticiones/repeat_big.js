function repeat_big(vector) {
    
    
    let contador = 0;
    let mayor = vector[0];

    for (let i = 0; i < vector.length; i++){
        if (vector[i] > mayor) {
            mayor = vector[i];
        }

    }

    for (let i = 0; i < vector.length; i++){
        if (vector[i] == mayor) {
            contador += 1;

        }
    }

    return mayor;

}
module.exports = repeat_big;