function position_big(vector) {
    
    
    let posicion = 0;

    let mayor = vector[0];

    for (let i = 0; i < vector.length; i++){
        if (vector[i] > mayor) {
            posicion = i;
            mayor = vector[i];
            
        }
    }

    return posicion;

}
module.exports = position_big;