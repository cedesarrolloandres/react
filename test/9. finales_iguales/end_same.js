function end_same(num1, num2, num3) {
    
    const digi1 = num1 % 10;
    const digi2 = num2 % 10;
    const digi3 = num3 % 10;
    
    if (digi1 == digi2 && digi2 == digi3){
        return 'son iguales';
    } else {
        return 'no son iguales';
    }

}
module.exports = end_same;