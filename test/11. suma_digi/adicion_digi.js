function adicion_digi(number) {

    let acum = 0;

    for (let i = 0; i < number.length; i++) {
        acum += parseInt(number[i]);
        
    }

    return acum;
    
}
module.exports = adicion_digi;