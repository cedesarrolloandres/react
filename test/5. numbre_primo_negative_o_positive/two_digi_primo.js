function two_digi_primo(number) {
    let count_positivo = 0;
    let count_negative = 0;

    if ((number > 9 & number < 100) || (number < -9 & number > -100)){

        if (number > 0){

            for (let i = 1; i <= number; i++){
                
                if (number % i == 0){
                    console.log(i)
                    count_positivo += 1;
                }

            }

            if (count_positivo == 2 || number == 2){
                return 'Es primo'
            } else {
                return 'No es primo'
            }

        } 

        else if (number < 0){
            for (let j = -1; j >= number; j--){
                if (number % j == 0){

                    count_negative += 1;

                }
            }

            if (count_negative == 2 || number == -2){
                return 'Es primo y es negativos'
            } else {
                return 'Es negativo, pero no es primo'
            }


        }

    }
        
}
module.exports = two_digi_primo;