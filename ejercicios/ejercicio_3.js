numero = 82;

if (numero > 9 && numero < 100){

    decena = parseInt(numero / 10);
    unidad = numero % 10;
    resul = decena + unidad;

    if (decena % 2 == 0 && unidad % 2 == 0){
        console.log('Ambos digitos son Pares');
    } else {
        console.log('Almenos 1 de los digitos no es Par')
    }

} else {
    console.log('Nùmero fuera de Rango');
}    