// Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se encuentra el número mayor. 
let vector = [3,6,7,2,5,4,7,9,6,2];
let posicion = 0;

let mayor = vector[0];

for (let i = 0; i < vector.length; i++){
    if (vector[i] > mayor) {
        posicion = i;
        mayor = vector[i];
        
    }
}

console.log(`El mayor es ${mayor} y esta en la posicion: ${posicion} del vector`);