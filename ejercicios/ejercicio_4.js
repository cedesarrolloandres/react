// Numeros de dos digitos menores que 20

let number = 13;
let acum = 0;

if (number > 9 && number < 20) {
    for (let i = 1; i <= number; i++){
        if (number % i == 0) {
            //console.log(i);
            acum = acum + 1; // si esto es mayor a 2 el numero no es primo
            
        }
    }

    //console.log(acum);

    if (acum == 2 || number == 2){
        console.log("Si es primo")
    } else {
        console.log("No es primo")  
    }

} else {
    console.log("Nùmero fuera de rango")
}