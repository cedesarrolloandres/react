// Leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro.
let number = 23;
let decena = 0;
let unidad = 0;

if (number > 9 && number < 100) {

    decena = parseInt(number / 10);
    unidad = number % 10;

    if (decena % unidad == 0 || unidad % decena == 0) {
        console.log('Al menos 1 digito es multiplo del otro')
    } else{
        console.log('No tiene ningùn multiplo del otro')
    }

}